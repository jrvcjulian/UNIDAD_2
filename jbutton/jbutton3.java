/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbutton;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author JULIAN
 */
   public class jbutton3 extends JFrame implements ActionListener{
    private JButton boton1,boton2;
    public jbutton3() {
        setLayout(null);
        boton1=new JButton("Varón");
        boton1.setBounds(10,10,100,30);
        boton1.addActionListener(this);
        add(boton1);
        boton2=new JButton("Mujer");
        boton2.setBounds(10,70,100,30);
        boton2.addActionListener(this);
        add(boton2);
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==boton1) {
            setTitle("Varón");
        }
        if (e.getSource()==boton2) {
            setTitle("Mujer");
        }
    }
    
    public static void main(String[] ar) {
        jbutton3 formulario1=new jbutton3();
        formulario1.setBounds(0,0,130,140);
        formulario1.setVisible(true);
    }
} 
